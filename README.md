This is a very basic weather app built with React.

This app is hosted at https://renepot.net/projects/weather

# Install

To install clone this repository to your local machine and then run `npm install` in the directory.

Once installation is complete, run `npm start` to start the weather app. It should automatically open the app in browser

# Build

`npm run build` will build the application to generate a static website that can be deployed to any (static) webhost.

# The app
The app will display the 5-day forecast of OpenWeatherAPI. You can edit the location of the forecast by putting in the new location in the input field. Make sure openWeatherAPI can resolve the location by using the format `[city],[country]`. If it cannot resolve it will display an error and the previous result will remain displayed.

# General notes
This React app was build in 3:15 hours with no prior knowledge of ReactJS besides finishing the Tic-Tac-Toe tutorial in about 45 minutes prior to developing this weather app. 

Further important note: This is the first web-based application I've build since 2015 so my html/css was a bit rusty. 

The 3:15 hours I've spent on this project excludes the ReactJS Tic-Tac-Toe tutorial.

The 3:15 hours includes refreshing memory on html/css and the inner workings of ReactJS. Also the writing of this readme is included in this. I've tracked my time using Toggl to make sure I track the time accordingly.

## Future improvements & features
Because of time constraint the app is very limited in usage. Some things that could be added to improve as-is:

- Use the location API to have a localized location as start instead of Edinburgh
- Allow for storing favorites in cookies/localStorage to easily switch between different cities
- Move the API key to a environment variable so it can be kept outside of the repository.
- Styling is not great.
  - Errors are not visually attractive
  - Spacing between the "hour-blocks" are not even everywhere
- Data is missing, an expand option to display more information would be amazing
- A graph library should be added so temperature (and other data variables like windchill, windspeed and humidity) can be plotted on an interactive graph instead of fixed blocks.
- Add mobile support, currently website width is hardcoded in css.

## Technical improvements
Currently technically this app is not very in-depth and is missing a couple things. 

- There is no testing
- Environment variables are not configured and therefore API key is hard-coded
- The app currently follows a happy-path. Besides a very basic check on API response it assumes data is structured as-is. This could cause run-time errors. "Optional Chaining" would be ideal here but is experimental. TypeScript might be able to help out on that level.
- When building the app the path for the JavaScript file is absolute. This should be configured to be relative when not hosted on the root of a domain (as with the hosted solution).
- Material-UI is used as a first choice, it might not be the best solution as I had no prior knowledge of ReactJS I chose what was popular.