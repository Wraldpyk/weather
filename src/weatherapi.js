/**
 * @TODO: key should be stored in a config file, excluded from repository and hosted on the server
 */
import Moment from 'moment';
const key = 'a8944d45a0b1f71c69fccf8632088dfe';
const url = 'https://api.openweathermap.org/data/2.5/forecast?q=%q%&units=Metric&appid=%appid%';

export default function fetchForecast(city) {
    return new Promise((resolve,reject) => {

        let fetchUrl = url;
        
        // replace query property with provided city & insert API key
        fetchUrl = fetchUrl.replace('%q%', city);
        fetchUrl = fetchUrl.replace('%appid%', key);
        
        fetch(fetchUrl).then(response => {
            return response.json()
        }).then(data => {
            let days = {};
            if (!data.list) {
                return reject();
            }
            // group data in days for rendering purposes
            data.list.forEach(item => {
                const day = Moment(item.dt_txt).format('DDDD');
                if (!days.hasOwnProperty(day)) days[day] = [];
                days[day].push(item);
            });
            delete data.list;
            data.days = days;
            resolve(data);
        });
    });
}