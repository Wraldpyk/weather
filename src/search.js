import React from "react";
import TextField from '@material-ui/core/TextField'

export default function Search(props) {
    return (
        <TextField  className="searchfield" 
                    id="outlined-basic" 
                    label="Search your city" 
                    variant="outlined"
                    onKeyDown={props.onKeyDown}
                    onChange={props.onChange}
                    value={props.value}
        />
    )
}
