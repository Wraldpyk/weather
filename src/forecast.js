import React from "react";
import Moment from 'react-moment';
import Paper from '@material-ui/core/Paper';
import Grid from '@material-ui/core/Grid';

export default class Forecast extends React.Component {
    render() {      
        if (!this.props.days || this.props.days.length === 0) return '';
        const dayCount = Object.keys(this.props.days).length;
        // loop through all days and make a day block
        return Object.keys(this.props.days).map((key, index) => {
            //const index = Object.keys(this.props.days).indexOf(key);
            return <Day data={this.props.days[key]} key={key} isFirst={index === 0} isLast= {index === dayCount - 1} />
        });
    }
}

class Day extends React.Component {
    render() {
        let justify = 'space-evenly';
        if (this.props.isFirst === true) justify = 'flex-end';
        if (this.props.isLast === true) justify = 'flex-start';
        return (
            <div>
                <Paper className="dayHeader"><Moment format="dddd, MMMM Do">{this.props.data[0].dt_txt}</Moment></Paper>
                <Grid container className="day" spacing={2} justify={justify}>
                    {this.props.data.map(block => {
                        return (
                            <Block key={block.dt} data={block}  />
                            )
                        })}
                </Grid>
            </div>
        )
    }
}

class Block extends React.Component {
    render() {
        return (
            <Grid item>
                <Paper className="dayBlock">
                    <Moment className="dayTime" format="HH:mm">{this.props.data.dt_txt}</Moment>
                    <span className="dayTemp">{Math.round(this.props.data.main.temp*10)/10}°C</span>
                    <img className="dayIcon" src={`https://openweathermap.org/img/wn/${this.props.data.weather[0].icon}.png`} />
                    <span className="dayDescription">{this.props.data.weather[0].description}</span>
                </Paper>
            </Grid>
        )
    }
}