import React from "react";
import ReactDOM from "react-dom";
import "./index.css";
import Search from './search';
import Forecast from './forecast';
import fetchForecast from './weatherapi';
import Paper from '@material-ui/core/Paper';

class Interface extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            location: 'Edinburgh,UK', // @TODO: define default based on browser location
            data: null,
            searchValue: 'Edinburgh,UK',
            error: false
        }
    }
    render() {
        // default fire the render
        let errorSpan;
        if (this.state.error) {
            errorSpan = <Paper className="error"><span className="errorText">{`Sorry, ${this.state.location} does not exist. Make sure you've entered country as well`}</span></Paper>
        }
        return (
            <div className="weather-container">
                <Search onChange={e => {this.handleChange(e)}} onKeyDown={e => {this.handleKeydown(e)}} value={this.state.searchValue} />
                {errorSpan}
                <Forecast className="forecast" days={this.state.data ? this.state.data.days: []}></Forecast>
            </div>
        );
    }

    componentDidMount() {
        this.renderForecast();
    }

    handleKeydown(e){ 
        if (e.key === 'Enter') {
            this.setState({"location": e.target.value}, () => {
                this.renderForecast();
            });
        }
    }

    handleChange(e) {
        this.setState({'searchValue': e.target.value})
    }

    renderForecast() {
        fetchForecast(this.state.location)
        .then(data => {
            console.log(data);
            this.setState({data: data, error: false});
        }).catch(e => {
            this.setState({error: true});
        });
    }
    
}




// ========================================

ReactDOM.render(<Interface />, document.getElementById("root"));

